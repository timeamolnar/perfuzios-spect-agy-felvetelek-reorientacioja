import numpy as np
import scipy.ndimage as nd
from skimage import morphology, filters
import maths_util
from volume_data.VolumeData import VolumeData
from sklearn.decomposition import PCA

eps = 0.001

class BrainVolume(VolumeData):

    def __init__(self, path, preproc=True, presegment=True):
        """
        :param path: path to the dicom file
        :param preproc: if True run some preprocessing steps - defaults to True
        :param presegment: if True runs segmentation upon init - defaults to True
        """

        super().__init__(path)

        if preproc:
            self.spatial_norm()
            self.norm_values()
            self.shift_center_to_o()
            self.low_noise_masking()

        self.isSegmented = False
        self.segmented_volume = None

        if presegment:
            self.segment()


    ### SEGMENTATION
    def get_segmentation(self):
        """
        segment the brain from non-brain regions in the data image
        :return: segmented brain volume
        """
        volume = np.copy(self.data)

        # to threshold with Otsu, only use non-zero pixels
        pixels = volume[volume > 0]
        otsu_threshold = filters.threshold_otsu(pixels, nbins=256)
        volume[volume < otsu_threshold] = 0

        erosion_strel = morphology.ball(2)
        dilation_strel = morphology.ball(5)

        volume = nd.binary_erosion(volume, erosion_strel)

        volume = nd.binary_fill_holes(volume)

        cm = nd.center_of_mass(volume)
        volume = maths_util.region_growing(volume, seed=(int(cm[0]), int(cm[1]), int(cm[2])))

        volume = nd.binary_dilation(volume, dilation_strel)
        volume = nd.binary_fill_holes(volume)
        volume = nd.binary_fill_holes(volume)

        to_mask = np.copy(self.data)
        to_mask[volume < 1] = 0

        return to_mask


    def segment(self):
        """
        get the segmentation of the brain and store in segmented_volume field
        also adjust the image so that the mean of the brain volume coordinates is in the center of the image
        :return: -
        """
        self.segmented_volume = self.get_segmentation()
        self.isSegmented = True

        # adjust center
        points = np.asarray(np.where(self.segmented_volume > eps)).T
        mean = np.mean(points, 0)
        self.shift_center_to_o(mean)
        o = np.asarray(self.data.shape) / 2
        self.segmented_volume = nd.shift(self.segmented_volume, shift=o - np.around(mean), order=0, cval=0.0, mode='constant')


    ### REORIENTATION
    def reorient(self):
        """
        get reorientation
        :return: zyx euler angles for rotating into correct position
        """
        msp_rotation_angles= self.get_msp_rotation()
        msp_rotation_angles[2] = 0
        msp_slice = self.get_msp_slice(msp_rotation_angles)
        ac_pc_rotation = self.get_ac_pc_rotation(msp_slice)

        return msp_rotation_angles[0], msp_rotation_angles[1], ac_pc_rotation


    ### MSP SEARCH
    def get_msp_rotation(self):
        """
        get rotation between the wanted place of MSP and the current estimated MSP
        :return:
        """
        msp, _ = self.search_msp()
        print('calculated msp is : ' + str(msp))

        original1 = [1, 0, 0]
        angles1 = maths_util.rotation_for_3d_vectors(msp, original1)

        original2 = [-1, 0, 0]
        angles2 = maths_util.rotation_for_3d_vectors(msp, original2)

        angles = angles1 if np.sum(np.abs(angles1)) < np.sum(np.abs(angles2)) else angles2

        print('msp rotation in angles: ' + str(angles))

        return angles


    def get_msp_slice(self, msp_rot=None):
        """
        get the sagittal slice on the MSP
        """
        angles = msp_rot if msp_rot is not None else self.get_msp_rotation()
        rotated = maths_util.rotate_image_zyx(self.segmented_volume, angles=angles, reshape=True)
        middle_slice_idx = round(rotated.shape[0]/2)
        msp_slice = rotated[middle_slice_idx, :, :]
        return msp_slice


    def search_msp(self):
        """
        get the msp
        :return: normalvector of estimated MSP
        """
        print('original shape is : ' + str(self.segmented_volume.data.shape))

        # msp estimate is initialized
        msp_estimate = self.get_initial_msp()

        downscale = self.pixel_spacing[0] < 2.9
        volume = self.downscale_3d() if downscale else self.segmented_volume
        if downscale:
            print('\noriginal image is downscaled, new shape is : \n' + str(volume.shape))


        old_msp = np.asarray([0, 0, 0])
        iterations = 0
        while np.sum(np.abs(msp_estimate-old_msp)) > 0.0001:
            old_msp = msp_estimate
            msp_estimate = maths_util.get_best_reflection_plane(self.segmented_volume, msp_estimate, 1)
            iterations += 1

        print('iterations of MSP adjustment: ' + str(iterations))

        return msp_estimate, iterations


    def get_initial_msp(self):
        """
        :return:    from the pca principal axes return the second component, this will define the initial MSP plane
        """
        assert self.isSegmented

        principal_axes = self.get_principal_axes()
        return principal_axes[1]


    def get_principal_axes(self):
        """
        calculate the principal axes of the segmented brain volume with PCA
        :return: 3 principal axes as vectors
        """
        assert self.isSegmented

        points = np.asarray(np.where(self.segmented_volume > eps)).T
        pca = PCA(3)
        pca.fit(points)
        return pca.components_


    ### AC-PC ORIENTATION
    def get_ac_pc_rotation(self, msp_slice=None):
        """
        calculate the rotation to adjust the plane paralell with AC-PC line
        :param msp_slice: 2D image, sagittal slice at the place of MSP
        :return: rotation angle around x axis of original 3D volume
        """
        if msp_slice is None:
            msp_slice = self.get_msp_slice()

        points = np.asarray(np.where(msp_slice > 0)).T
        pca = PCA(2)
        pca.fit(points)

        ac_pc_orientation = pca.components_[0]

        print('ac pc orientation is : ' + str(ac_pc_orientation))

        angle = maths_util.rotation_for_2d_vectors(ac_pc_orientation, [-1, 0]) *-1
        if -90 > angle > -180:
            angle = angle + 180
        if 90 < angle < 180:
            angle = angle - 180

        print('ac pc rotation angle is : ' + str(angle))

        # rotation to be applied after msp rotation, plane of AC-PC is rotated around x axis
        return angle


    ### OTHER HELPER FUNCTIONS
    def downscale_3d(self, sigma=1.75, new_shape=(0.5, 0.5, 0.5), use_original=False):
        """
        :param sigma: measure of gaussian smoothing before the downscaling
        :param new_shape: measure of downscaling, tuple with 3 elements for the 3 dimensions
        :param use_original: downscale the original data not the segmented - defaults to False
        :return: downscaled image of the segmented brain volume
        """
        vol = self.segmented_volume if self.isSegmented and not use_original else self.data
        new = nd.filters.gaussian_filter(vol, sigma=sigma)
        new = nd.zoom(new, new_shape)
        return new


    def rotate_segmentation(self, angles):
        """
        used for creating figures only
        :param angles: z, y, x rotation angles
        :return:
        """
        assert self.isSegmented

        tmp = maths_util.rotate_image_zyx(self.segmented_volume, angles)
        self.segmented_volume = tmp