import numpy as np


def results():
    rotation_xyz = np.load('./rotation_xyz_angles.npy')
    reorienter_zyx = np.load('./reorienter_zyx_angles.npy')

    rotation_xyz = np.flip(rotation_xyz, 1)
    rotation_xyz *= -1

    differences = rotation_xyz - reorienter_zyx
    abs_diff = np.abs(differences)
    mean = np.mean(abs_diff, 0)
    print(mean)

if __name__ == '__main__':
    results()