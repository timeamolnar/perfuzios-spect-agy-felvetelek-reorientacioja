import test.files_util as util
import pydicom
from itertools import product
import maths_util
from BrainVolume import BrainVolume as BV
import numpy as np
import time


def msp_searches():
  files = util.TestFiles('../correctly_oriented_images/')
  rotation_angles = list(product([-10, 10], repeat=3))
  rotation_angles.extend(list(product([-20, 20], repeat=3)))
  print('number of rotations per image : ' + str(len(rotation_angles)))

  iters = []
  exec_times = []

  rotation_number = len(rotation_angles)
  file_number = len(files.filePaths)
  num_range = rotation_number * file_number
  for current in range(num_range):
    print('\nrunning test iteration: ' + str(current + 1) + '/' + str(num_range))
    print('***************************************************')

    scan = pydicom.dcmread(files.filePaths[current % file_number])
    data = scan.pixel_array
    rot_xyz = rotation_angles[current % rotation_number]
    data = maths_util.rotate_image_xyz(data, rot_xyz, order=3)

    scan.PixelData = data.tostring()
    scan.NumberOfFrames, scan.Rows, scan.Columns = data.shape
    scan.Frames = scan.NumberOfFrames

    scan.save_as('./tmp.dcm')

    bv = BV('./tmp.dcm')
    start_time = time.time()
    _, iter_num = bv.search_msp()
    end_time = time.time()
    exec_times.append(end_time-start_time)
    iters.append(iter_num)
    print('***************************************************')

  x = np.asarray(iters)
  np.save('./iters.npy', x)
  print('avg iteration number is: ' + str(np.mean(x)))

  t = np.asarray(exec_times)
  np.save('./msp_exec_times.npy', t)

if __name__ == '__main__':
    msp_searches()